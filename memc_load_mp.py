#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import gzip
import sys
import glob
import logging
import collections
from optparse import OptionParser
import appsinstalled_pb2
import memcache
import multiprocessing as mp
from itertools import islice


NORMAL_ERR_RATE = 0.01
BATCH_SIZE = 10000
RETRIES = 3
SOCKET_TIMEOUT = 1
WORKERS_DEFAULT = 1
AppsInstalled = collections.namedtuple("AppsInstalled", ["dev_type", "dev_id", "lat", "lon", "apps"])


def dot_rename(path):
    head, fn = os.path.split(path)
    # atomic in most cases
    os.rename(path, os.path.join(head, "." + fn))


def insert_appsinstalled(memc_addr, memc_clients, appsinstalled, dry_run=False):
    ua = appsinstalled_pb2.UserApps()
    ua.lat = appsinstalled.lat
    ua.lon = appsinstalled.lon
    key = "%s:%s" % (appsinstalled.dev_type, appsinstalled.dev_id)
    ua.apps.extend(appsinstalled.apps)
    packed = ua.SerializeToString()
    try:
        if dry_run:
            logging.debug("%s - %s -> %s" % (memc_addr, key, str(ua).replace("\n", " ")))
        else:
            memc = memc_clients[memc_addr]
            ok = False
            attempt = 0
            while not ok and attempt < RETRIES:
                ok = memc.set(key, packed)
                attempt += 1
    except Exception as e:
        logging.exception("Cannot write to memc %s: %s" % (memc_addr, e))
        return False
    return True


def parse_appsinstalled(line):
    line_parts = line.strip().split("\t")
    if len(line_parts) < 5:
        return
    dev_type, dev_id, lat, lon, raw_apps = line_parts
    if not dev_type or not dev_id:
        return
    try:
        apps = [int(a.strip()) for a in raw_apps.split(",")]
    except ValueError:
        apps = [int(a.strip()) for a in raw_apps.split(",") if a.isidigit()]
        logging.info("Not all user apps are digits: `%s`" % line)
    try:
        lat, lon = float(lat), float(lon)
    except ValueError:
        logging.info("Invalid geo coords: `%s`" % line)
    return AppsInstalled(dev_type, dev_id, lat, lon, apps)


def create_batches(file, batch_queue):
    logging.info('Processing %s' % file)
    with gzip.open(file, 'rt') as fd:
        while True:
            batch = list(islice(fd, BATCH_SIZE))
            if not batch:
                break
            batch_queue.put((file, batch))
    batch_queue.put((file, ['quit']))


def process_batch(batch, device_memc, memc_clients, options):
    errors, processed = 0, 0
    for line in batch:
        line = line.strip()
        if not line:
            continue
        appsinstalled = parse_appsinstalled(line)
        if not appsinstalled:
            errors += 1
            continue
        memc_addr = device_memc.get(appsinstalled.dev_type)
        if not memc_addr:
            errors += 1
            logging.error("Unknow device type: %s" % appsinstalled.dev_type)
            continue
        ok = insert_appsinstalled(memc_addr, memc_clients, appsinstalled, options.dry)
        if ok:
            processed += 1
        else:
            errors += 1
    return errors, processed


def run(batch_queue, device_memc, options, file_nums, processed_stat, errors_stat):
    memc_clients = dict((address, memcache.Client([address], socket_timeout=SOCKET_TIMEOUT))
                        for _, address in device_memc.items())
    while True:
        file, batch = batch_queue.get()
        if not batch:
            return
        elif batch[0] == 'quit':
            dot_rename(file)
        else:
            errors, processed = process_batch(batch, device_memc, memc_clients, options)
            file_num = file_nums[file]
            processed_stat[file_num] += processed
            errors_stat[file_num] += errors


def main(options):
    device_memc = {
        "idfa": options.idfa,
        "gaid": options.gaid,
        "adid": options.adid,
        "dvid": options.dvid,
    }
    batch_queue = mp.Queue()
    files = list(glob.iglob(options.pattern))
    file_nums = {file: file_num for file_num, file in enumerate(files)}
    processed_stat = mp.Array('i', [0] * len(files))
    errors_stat = mp.Array('i', [0] * len(files))
    parsers = []
    for _ in range(options.workers):
        p = mp.Process(target=run, args=(batch_queue, device_memc, options,
                                         file_nums, processed_stat, errors_stat))
        p.start()
        parsers.append(p)
    for file in files:
        create_batches(file, batch_queue)
    for _ in range(options.workers):
        batch_queue.put(('', list()))
    for p in parsers:
        p.join()
    for file, file_num in file_nums.items():
        errors = errors_stat[file_num]
        processed = processed_stat[file_num]
        if not processed:
            continue
        err_rate = float(errors) / processed
        if err_rate < NORMAL_ERR_RATE:
            logging.info("File: {}: Acceptable error rate {}. Successfull load".format(file, err_rate))
        else:
            logging.error("File: {}: High error rate ({} > {}). Failed load".format(file,
                                                                                    err_rate, NORMAL_ERR_RATE))


def prototest():
    sample = "idfa\t1rfw452y52g2gq4g\t55.55\t42.42\t1423,43,567,3,7,23\ngaid\t7rfw452y52g2gq4g\t55.55\t42.42\t7423,424"
    for line in sample.splitlines():
        dev_type, dev_id, lat, lon, raw_apps = line.strip().split("\t")
        apps = [int(a) for a in raw_apps.split(",") if a.isdigit()]
        lat, lon = float(lat), float(lon)
        ua = appsinstalled_pb2.UserApps()
        ua.lat = lat
        ua.lon = lon
        ua.apps.extend(apps)
        packed = ua.SerializeToString()
        unpacked = appsinstalled_pb2.UserApps()
        unpacked.ParseFromString(packed)
        assert ua == unpacked


if __name__ == '__main__':
    op = OptionParser()
    op.add_option("-t", "--test", action="store_true", default=False)
    op.add_option("-l", "--log", action="store", default=None)
    op.add_option("--dry", action="store_true", default=False)
    op.add_option("--pattern", action="store", default="./data/appsinstalled/*.tsv.gz")
    op.add_option("--idfa", action="store", default="127.0.0.1:33013")
    op.add_option("--gaid", action="store", default="127.0.0.1:33014")
    op.add_option("--adid", action="store", default="127.0.0.1:33015")
    op.add_option("--dvid", action="store", default="127.0.0.1:33016")
    op.add_option("--workers", action="store", type=int, default=WORKERS_DEFAULT)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO if not opts.dry else logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    if opts.test:
        prototest()
        sys.exit(0)

    logging.info("Memc loader started with options: %s" % opts)
    try:
        main(opts)
    except Exception as e:
        logging.exception("Unexpected error: %s" % e)
        sys.exit(1)
