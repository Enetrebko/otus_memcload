# Multiprocess version of MemcLoad


### Run example

```bash
python3 memc_load_mp.py --pattern='./data/appsinstalled/*.tsv.gz' --workers=10
```

### Performance Test

1 worker: 5m 42s

5 workers: 2m 55s

10 workers: 1m 16s